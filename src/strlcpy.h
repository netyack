/*
 * netyack
 * By Daniel Borkmann <daniel@netyack.org>
 * Copyright 2009, 2010 Daniel Borkmann.
 * Subject to the GPL.
 */

#ifndef STRLCPY_H
#define STRLCPY_H

extern size_t strlcpy(char *dest, const char *src, size_t size);

#endif /* STRLCPY_H */
